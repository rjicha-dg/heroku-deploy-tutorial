# Automaticky deploy na Heroku

1. Zalozit projekt <PROJECT_NAME> na Heroku
2. update requirements.txt (whitenoise, dj-database-url, django-heroku, gunicorn)
3. pridat .gitlab.ci.yml
4. pridat settings_prod.py
5. pridat Procfile (nastavte si spravne cesty a nazvy)
6. zalozit databazi na heroku (Resources - Add-ons - Heroku Postgres)
7. zalozit env promennou na gitlabu - HEROKU_API_KEY (GitLab - Settings - CI/CD - Variables). 
   Hodnotu klice ziskate na Heroku (https://dashboard.heroku.com/account) - az uplne dole
8. upravit settings.py a pridat STATIC_ROOT
9. ulozit a odeslat do repozitare
10. pockat az zezelena indikator CI/CD a pak mrknout na vysledek (https://dg-deploy-tutorial.herokuapp.com/)